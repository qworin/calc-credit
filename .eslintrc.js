module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: 'babel-eslint',
  rules: {
    curly: ['error', 'multi-line'],
  },
};
