/* global Intl */
import lang from '../resources/lang';

const isInvalid = date => Number.isNaN(+new Date(date));

const toValue = date => new Date(date).toISOString().replace(/T.+$/, '');

const toDate = value => (isInvalid(value) ? null : new Date(value));

const FORMAT = {
  DAY: 'DD',
  DOT: 'DD.MM.YYYY',
  SHORT_DOT: 'DD.MM',
  MONTH: 'MM.YYYY',
  LONG: 'D MMM',
  LONG_MONTH: 'MMM YYYY',
  FULL: 'D MMM YYYY',
};
const formatDot = value =>
  value
    .split('-')
    .reverse()
    .join('.');
const formatIntl = params => x => {
  const formatter = Intl.DateTimeFormat(lang.locale, params);
  try {
    const parts = formatter.formatToParts(toDate(x));
    return Object.keys(params)
      .map(key => parts.find(({type}) => type === key).value)
      .join(' ');
  } catch (error) {
    console.log(error);
    return '';
  }
};
const format = {
  [FORMAT.DAY]: x => toValue(x).replace(/^\d{4}-\d{2}-/, ''),
  [FORMAT.DOT]: x => formatDot(toValue(x)),
  [FORMAT.SHORT_DOT]: x => formatDot(toValue(x).replace(/^\d{4}-/, '')),
  [FORMAT.MONTH]: x => formatDot(toValue(x).replace(/-\d{2}$/, '')),
  [FORMAT.LONG]: formatIntl({day: 'numeric', month: 'short'}),
  [FORMAT.LONG_MONTH]: formatIntl({month: 'short', year: 'numeric'}),
  [FORMAT.FULL]: formatIntl({day: 'numeric', month: 'short', year: 'numeric'}),
};

const formatDate = (valueOrDate, type = FORMAT.DOT) => {
  if (isInvalid(valueOrDate)) return '';
  const fn = format[type] || formatDot;
  return fn(valueOrDate);
};

export {
  toDate as valueToDate,
  toValue as dateToValue,
  formatDate,
};
