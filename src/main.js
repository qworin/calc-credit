import Credit from './utils';

const rou = v => Math.round(100 * v) / 100;

const calcPaymentTable = ({sum: initSum, rate: rateYr, term: termYr, date: initDate, earlies = []}) => {
  const credit = Credit(rateYr, initDate);
  let term = 12 * termYr;
  let payCount = 0;
  let sum = initSum;
  let annuity = credit.calcAnnuity(sum, term);
  // let date = initDate;
  const cost = annuity * term;

  const result = [...Array(term)].reduce((res) => {
    const prev = res[res.length - 1] || {};
    let start = credit.date || initDate;
    const end = credit.getNextMonth(start);
    const currEarly = !prev.early && credit.findEarlyInMonth(earlies, start);
    currEarly && currEarly.forEach((early) => {
      console.log(early, {start, end: early.date})
      const perc = credit.calcPerc(sum, start, early.date);
      const pay = early.sum;
      const body = pay - perc;
      res.push({date: early.date, main: sum, perc, body, pay, early: true});
      sum -= body;
      if (!early.decTerm) {
        term -= payCount;
        annuity = credit.calcAnnuity(sum, term);
        payCount = 0;
      } else {
        term = payCount + credit.calcTerm(sum, annuity);
      }
      start = early.date;
    });
    const perc = credit.calcPerc(sum, start, end);
    const {pay, body} = ((pay = annuity, body = sum) => {
      if (annuity < sum) return {pay, body: pay - perc};
      return {pay: body + perc, body};
    })();
    // NB order matters
    credit.getNextMonth();
    if (sum > 0) res.push({date: end, main: sum, perc, body, pay});
    sum -= body;
    payCount += 1;
    return res;
  }, []);
  console.table(result.map(datum => (
    Object.entries(datum).reduce((r, [k, v]) => ({
      ...r,
      [k]: typeof v === 'number' ? rou(v) : v,
    }), {})
  )))
  const payments = result.length - earlies.length;
  const {total, totPerc, totBody} = result.reduce((res, {pay, perc, body}) => {
    res.total += pay;
    res.totPerc += perc;
    res.totBody += body;
    return res;
  }, {total: 0, totPerc: 0, totBody: 0});
  return {payments, payCount, cost, total, term, totPerc, totBody, profit: cost - total};
};

console.log(calcPaymentTable({
  sum: 1e6,
  rate: 0.12,
  term: 1.5,
  date: '2013-02-01',
  earlies: [
    {date: '2013-05-10', sum: 108e3},
    {date: '2013-12-15', sum: 9e4, decTerm: true},
    {date: '2014-02-10', sum: 65e3},
  ]
}));

// credits:
// dec sum: https://mobile-testing.ru/loancalc/rachet_dosrochnogo_pogashenia/
// dec term: https://mobile-testing.ru/dosrochnoe_pogashenie_ismenenie_sroka/
