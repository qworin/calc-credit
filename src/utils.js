import moment from 'moment';

const FMT = 'YYYY-MM-DD';
const DAYS = 365;

const getNextMonth = date => moment(date).add(1, 'month').format(FMT);
const daysInYear = date => DAYS + +moment(date).isLeapYear();

const partOfYear = (start, end = getNextMonth(start)) => {
  if (moment(start).isSame(end, 'year')) {
    return moment(end).diff(start, 'days') / daysInYear(end);
  } else {
    const startTo = moment(start).endOf('month').format(FMT);
    const endFrom = moment(end).startOf('month').format(FMT);
    const startDiff = start === startTo ? 1 : moment(startTo).diff(start, 'days');
    const endDiff = end === endFrom ? 1 : moment(end).diff(endFrom, 'days')
    return startDiff / daysInYear(start) + endDiff / daysInYear(end);
  }
};
const findEarlyInMonth = (earlies, start, end = getNextMonth(start)) => earlies.reduce(
  (res, early) => {
    const {date} = early;
    if (moment(date).isBetween(start, end) || [start, end].includes(date)) res.push(early);
    return res;
  },
  [],
);

const Credit = (annualRate, initDate) => {
  const rate = annualRate / 12;
  const log = (x, base) => Math.log(x)/Math.log(base);
  const credit = {
    date: initDate,
    calcAnnuity: (sum, term) => sum * rate * Math.pow(1 + rate, term) / (Math.pow(1 + rate, term) - 1),
    // reverse calcAnnuity
    calcTerm: (sum, annuity) => Math.ceil(log(annuity/(annuity - rate * sum), 1 + rate)),
    calcPerc: (sum, start, end) => sum * annualRate * partOfYear(start, end),
    findEarlyInMonth,
    getNextMonth: (date) => {
      if (date) return getNextMonth(date);
      credit.date = getNextMonth(credit.date);
    },
  };
  return credit;
}

export default Credit;
